pragma solidity >=0.4.24 <0.6.0;

// Import the Base class and other dependencies
import "./interface.sol";
import "./organ.sol";

// New contract for entity
contract entity is organBrowser {
    
    // Owner
    address contractOwner 
    
    // Declaration of struct for Entity of donators
    struct donator {
        // Entities firstname
        private string firstName;   
        // Entities lastname
        private string lastName;   
        // Entities date of birth
        private uint256 birthDate;
        // Entites date of death
        private uint256 deathDate;
        // Agreement to donate organs
        private bool agreement;
        // Trusted doctors are people who can change the state of 
        private bytes32[] trustedPersons;
        // Related organs
        private bytes32[] relatedOrgans;
        // The bool to determine, if a person is alive
        private bool alive = true;
    }
    
    // Create mapping for donators
    mapping (address => donator) public donators;
    
    // Create Array for donators
    address[] public donatorsAccounts;
    
    // Declaration of struct for Entity of trust
    struct trustedPerson {
       string firstName;
       string lastName;
    }
    
    // Create mapping for trusted persons
    mapping (address => trustedPerson) public trustedPersons;
    
    // Create Array for trusted persons
    address[] public trustedAccounts;
   
    // Initial Constructor
    constructor () public {
        // First sender is owner
        contractOwner == msg.sender;
    }
   
    // Only actions for the owner 
    modifier entityCheck() {
        // Simple check to see if address exists
        donators[msg.sender]
        
        
        
       require(msg.sender == patientAddress);
       _;
    }
   
    // Check if sender address is owner or trusted
    modifier entityRelation() {
        // If condition to loop thorugh trustedPersons
        if (msg.sender != patientAddress) {
            // Sender must be a trusted person
            require(trustedPersons[msg.sender], "You are not trusted, action denied!");
        } else {
            // Check if sender is owner
             require(msg.sender == owner, "You are not the owner, action denied");
        }
       _;
    }
    
    // Function to add a new donator
    function addDonator (string _firstName, string _lastName, uint256 _birthDate, uint256 _deathDate, bool _agreement) public {
        // Load donator
        var donatingPerson = donators[msg.sender];
        
        // Fill in information
        donatingPerson.firstName = _firstName;
        donatingPerson.lastName = _lastName;
        donatingPerson.birthDate = _birthDate;
        donatingPerson.deathDate = _deathDate;
        donatingPerson.agreement = _agreement;
        
        // Create reference
        donatorsAccounts.push[msg.sender] -1;
    }
    
    // Function to add a new trusted person (Owner only)
    function addTrustedPerson (address _address, string _firstName, string _lastName) public payable {
        
        // Charge the entity for multiple Relations - The first (Primar) relation is free
        if (donatorsAccounts[msg.sender].trustedPersons.length == 0) {
            // First trust is free
            require(msg.value == 0 ether, "Your first trusted Person is free!");
        } else {
            // If it's not the first trusted
            require(msg.value == 0.001 ether, "Adding more than one trusted person costs 0.001 ether!");
        }
        
        // Load trusted person
        trustedPerson storage trustingPerson = trustedPersons[_address];
        
        // Fill in information
        trustingPerson.firstName = _firstName;
        trustingPerson.lastName = _lastName;
        
        // Create reference
        trustedAccounts[trustedAccounts.length-1].push[trustingPerson];
        donators[msg.sender].trustedPersons.push(_address) -1;
    }

    // Function to change donations agreement
    function changeAgreement (bool _agreement) public {
        // change the state
        donators[msg.sender].agreement = _agreement;
    }

   
   // Change patient state (death)
   function changeAlive(address _donatorAddress, uint256 _deathDate) public {
       // Change the state of the patient (after death)
       donators[_donatorAddress].alive = false;
       donators[_donatorAddress].deathDate = _deathDate;
       
      // Set organs on state available
      uint ArrayIndex = donators[_donatorAddress].relatedOrgans.length;
      
      // Loop over all organs 
      for (uint i=ArrayIndex; i>=1; i--) {
          // Get the id of the Organ
          organList[donators[_donatorAddress].relatedOrgans[ArrayIndex-1]].state = "Available";
      }
   }
   
   // --- Organ functions --- //
   // Function to read organ list
   function readOrganList(address _address) public payable returns(uint) {
       // Require message value 
       require(msg.value == 0.1 ether, "Viewing tracking history costs 0.1 ether!");
       
       // Send payment
       contractOwner.transfer(msg.value);
       
       // Read record for all organs
       uint ArrayIndex = organs.length;
       for (uint i = ArrayIndex-1; i >= 1; i--) {
           // Get List or something like that
           getOrganList(i);
       }
   }
   
   // Function to get all donators
   function getOrganList(uint i) internal view returns (organ) {
       // Return as organ object
       return organs[i];
   }
}
