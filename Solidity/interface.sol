pragma solidity >=0.4.24 <0.6.0;

// Create interface to declare the main functions
interface organBrowser {
    
    // Track an organ on it's specific Id 
    function getOrgan(organId) external payable;
    
    // View the waiting list of organs based on the type of organ
    function viewWaitingList(organType) external payable;
    
    // View your own Entity
    function viewEntity(entityId) external;
    
    // Change attributes of an entity
    function changeEntity(entityId) external;
}