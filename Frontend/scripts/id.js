var ids = [];

$('#submit-id-data').on('submit', (e) => {
    e.preventDefault(e);
    let forename = $('input[name=forename]').val();
    let surname = $('input[name=surname]').val();
    let birthDate = $('input[name=birth-date]').val();
    let id = $('input[name=idset]').val();
    wholeId = ({
        id: id,
        forename: forename,
        surname: surname,
        birthDate: birthDate
    });
    ids.push(wholeId);
});

$('#find-person').on('submit', (e) => {
    e.preventDefault();
    entered_id = $('input[name=id-get]').val();
    for(var i = 0; i < ids.length; i++) {
        if(entered_id == ids[i].id){
            $('#id-sheet').html(`
            <p>Forename: ${ids[i].forename}</p>
            <p>Surname: ${ids[i].surname}</p>
            <p>Birth Date: ${ids[i].birthDate}</p>
            <p>Id: ${ids[i].id}</p>

            `);
            return
        }
    }
});