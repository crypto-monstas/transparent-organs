# Crypto Monstas

IIFF Cryptochicks Project

## What's it about
In this chapter we will try to cover to planed output of this project. Also what visions we won't be able to cover but would be part of the whole idea.


### Hackaton time (16.02.2019)
To see what we had initial in mind we built this chapter. **This Project will be further developed but not within this Hackaton!**

#### What we solve

At any time at any place on earth someone is looking for an organ. There are also people selling their Kidneys for so cheap only that they are able to feed their family. There are terrorist groups which sell organs from freshly killed people. Someone with a lot of money and in need for an organ will pay corrupt doctors to be the first ones to receive an organ and skiping the whole waiting list. 

And we all thought that's just not okay and a big call for the blockchain!

#### First Sketches

That was our first sketch for the overall idea:

![First Sketch](./media/sketches/sketch-1.jpg "First Sketch")

#### Presentation Pitch

We went over different ideas during this day and as a result came up with this. We didn't have too much time to create the presentation. But we summarized the most important things within the presentation.

Take a look at our presentation here:

![Presentation Pitch](./media/presentation_pitch/Secure_&_transparent_organ_transplantation.pdf "Presentation Pitch")

#### Further explained & Vision

We wanted to build the most part within one smart contract. That means, that donators would register themselves als donators on the blockchain. They could add trusted personen, which could interact with the data of the donator to a certain extend. Also in our simple case we would register for each donotr three organs on the blockchain: the **lung**, the **kidneys** and the **heart**. You would create a relation between each organ and each donator. After Someone would be registerd as death, the organs would be released and tracking operations would be related to the organ.
Our service is to browse all the checkpoints for one organ and to see it's origin. That's where we would charge money. 

### Scrapped idea(s)

**Smart Resource Rents:**

The subject is booking of objects. By refering to an object a flat, a house or anything that's made for a guest to stay is meant. But also it can't just be a normal object but a smart object. That's means that the object contains resource gadgets, which could interact with a Smart Contract (This won't be realized within this project). With this we adress temporary bookings of objects (such as objects on AirBnB). The current situation with booking an object (flat/house/etc.) is, that you will pay a fixed price for each night or for the entire time you are planing to stay. The price is calculated with different aspects from the owner of the object. Once value that varies the most, is the usage of resource for one guest. An owner has to take the average usage of previous guests and add that to the price for the whole object. If a guest consumes less than this average of resources, he is paying too much for the object because his usage differs from the average. If a guest consumes more than this average of resources, the owner must pay the outstanding costs for the resources consumed on top of the average. As a logical result it makes sense for the owner the calculate and demand a higher average value for the resources to prevent from paying them out of his own pocket. And as a consequence the price for an object get higher for the guest.
For the future it's important to take care with resources. There for we thought about a concept regarding this booking situation. Our idea is, that resources aren't included in the booking price of an object. Instead the guest pays them as he makes usage of a kind of resource. So for example, if the guest turns the light on the booked object, all the resources needed to fullfill this action must be paid by the issuer (in this case the guest). And this is meant for each usage of a specific resource. The booking for the object would be held in an escrow contract till the stay of the guest is over. In this way you can seperate the actual contract for the booking of the flat separte from usage of the resources by the guest. In this way the owner doesn't have to speculate prices and overprice his object and the guest hasn't to pay as much just for the booking. But the guest is in this responsible for his consumption of resources. 

By seperating the booking contract and the consumption of resource you can cover the following **case**: If a guest isn't happy with the object he might be demand a full refund or reduction for his booking. But in our opinion the resources that have been used in this span should still be payed. With the described Idea this would be the case because it's directly paid after usage. Therefor the owner won't have to pay suchs costs out of his pockets.

You could take this a step further and make usage of anything (for example taking the elevator) directly paid by the issuer. This way you could avoid city taxes for foreign visitors. But this topic won't be covered within this project.

![Overall vision diagramm](./media/diagrams/graphics/smart_resources_roadmap_vision.png "Overall vision diagramm")

__This idea was scrapped because it is too generic and didn't serve a specific Use-Case.__

## Project Tools

### Git

Obviously we are using git. If you are not familiar with git you will find here the most used commands in a quick overview.
These commands count for the Git Bash client [https://git-scm.com/downloads](https://git-scm.com/downloads). 

Clone a project:

    git clone git@gitlab.com:crypto-monstas/transparent-organs.git

Change to a **new** branch (if not exists):

    git checkout -B who-dev


Add your changes for the next commit:

    git add -A

Commit all added changes:

    git commit -am "What is in this commit"
	
Push your latest commit to the origin:

    git push origin who-dev (-f)
	
Rebase (squash) your commits against master:

    git rebase -i master

With these commands you have everything you need. **Note:** We don't push directly to master (restricted). Bringing changes to master is allowed over Merge(Pull) Requests. 

### Gitlab-CI

With GitLab CI we are able to fullfill CI within our project.

#### Frontend
Everytime you push to this central repository the content of ./Frontend is uploaded to a webserver.
That way you don't need a local webserver. Also only changed files will be uploaded

**Feature Branch**: Uploaded to **[https://www.dev.buttahtoast.ch/BRANCH](https://www.dev.buttahtoast.ch/)** or **[https://dev.buttahtoast.ch/](https://dev.buttahtoast.ch/)**

**Example**: If your branch is called **my-branch** you will find your uploaded code under **https://www.dev.buttahtoast.ch/my-branch**

**Master Branch**: Uploaded to **[https://www.prod.buttahtoast.ch/](https://www.prod.buttahtoast.ch/)** or **[https://prod.buttahtoast.ch/](https://prod.buttahtoast.ch/)**

### Discord
This Project is connected to a Discord T-Channel. There you will all events (https://discord.gg/2cBtpy#gitlab-events).

## Knowhow

How to Deploy a DAPP on a POA network:
[https://beta.kauri.io/article/549b50d2318741dbba209110bb9e350e/poa-part-1-develop-and-deploy-a-smart-contract](https://beta.kauri.io/article/549b50d2318741dbba209110bb9e350e/poa-part-1-develop-and-deploy-a-smart-contract)

## Recap



